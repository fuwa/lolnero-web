## Alpine 3.14

```
sudo apk add build-base cmake git
sudo apk add boost-dev readline-dev libsodium-dev openssl-dev rapidjson-dev

git clone https://gitlab.com/lolnero/lolnero.git

mkdir lolnero/build
cd lolnero/build

cmake .. && make
```

Generated binaries will be in `./bin/`.


## Arch

```
sudo pacman -S base-devel cmake git
sudo pacman -S boost readline libsodium openssl rapidjson

git clone https://gitlab.com/lolnero/lolnero.git

mkdir lolnero/build
cd lolnero/build

cmake .. && make
```

Generated binaries will be in `./bin/`.


## Debian 11 "Bullseye"

```
sudo apt install build-essential cmake git

sudo apt install libboost-dev

sudo apt install \
libboost-program-options-dev \
libboost-serialization-dev \
libboost-system-dev

sudo apt install \
libreadline6-dev \
libsodium-dev \
libssl-dev \
rapidjson-dev

git clone https://gitlab.com/lolnero/lolnero.git

mkdir lolnero/build
cd lolnero/build

cmake .. && make
```

Generated binaries will be in `./bin/`.


## Gentoo

```
sudo emerge \
dev-util/cmake \
dev-vcs/git

sudo emerge \
dev-libs/boost \
sys-libs/ncurses \
dev-libs/libsodium \
dev-libs/openssl \
dev-libs/rapidjson

git clone https://gitlab.com/lolnero/lolnero.git

mkdir lolnero/build
cd lolnero/build

cmake .. && make
```

Generated binaries will be in `./bin/`.


## Build for the built-in OpenCL miner

1. Install [`opencl-headers`][1]
2. Install [`ocl-icd`][2]
3. For AMD GPUs, install [`rocm-opencl-runtime`][3]. For NVIDIA GPUs, install [`opencl-nvidia`][4].
4. In the last command that involves `cmake`, do 

        cmake -DUSE_OPENCL=ON .. && make


[1]: https://archlinux.org/packages/?name=opencl-headers
[2]: https://archlinux.org/packages/?name=ocl-icd
[3]: https://aur.archlinux.org/packages/rocm-opencl-runtime/
[4]: https://archlinux.org/packages/?name=opencl-nvidia
